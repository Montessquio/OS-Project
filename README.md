# OS-Project
A learning experience for me on learning Operating Systems Development

This isn't expected to be actually used or anything, it's just an experiment for me to learn the internals of an OS as well as the Rust Language.

# Documentation
My personal documentation on this OS, as well as some informal lists, within the [doc folder](doc/)
