# Microkernel Design ideas.

# Trust Circles.
In order to facilitate secure and still fast communication between modules or even programs,
trust circles may be used.

In the program's header, or in the modules' header, there will be some code describing other
modules or programs that it trusts. Only other modules that have the exact same trust parameters
(i.e. their trust circle has the exact same members as all other members) will be able to use
unchecked communication between them.

### Example
```
Foo_Module HEADER = self, Bar_module
Bar_Module HEADER = self, Foo_module
Baz_Module HEADER = self, Bar_Module
```

In this example, `Foo` and `Bar` would be in a circle of trust, and be able to communicate without checks.

`Bar` and `Baz` are in a different circle, and thus are allowed to communicate the same way.

`Foo` and `Baz` do not have matching header signatures, so they cannot communicate directly.
However, if `Bar` exposes a method to both of them allowing them to communicate, they may
through `Bar`, but not directly.

```
`Foo` <=> `Bar` <=> `Baz`

`Foo` =/= `Baz`
```

# Filesystems and modules.
There will actually be two filesystems.
First, an EXT2 filesystem that contains a kernel filesystem, where boot-time modules and drivers
will be kept.

From there, a post-boot module should provide a userspace filesystem.
Whether or not this is different from the Kernel FS is up to the implementation module.


# kShell
The kernel Shell is what the user is first dropped into on boot, if no kernel init file is found or if it is corrupt.

The user may enter a few basic commands to load into another module.

 - `list` - will list all registered modules and their load state.
            If the FS is erroring, it will only show currently loaded (in memory) modules.
 - `load <mod>` - will load the specified module according to the module name.
 - `drop <mod>` - drops into the selected module's shell, if it provides one.
 - `stop <mod>` - Once a module has been loaded, it must be shut down from within it's shell in order to
get a clean exit. You may use `stop <mod>` to forcefully shut it down.
