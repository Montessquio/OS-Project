#!/usr/bin/python3

"""
Takes The Key codes in keyCodes.txt and
re-exports them into a text file ready for
insertion into rust.
"""

with open("keyCodes.txt") as f:
    content = f.readlines()
content = [x.strip() for x in content] # strip extra characters

for x in content:
    s = x.split(" ")
    s[0] = s[0].replace(' ', '')
    s[1] = s[1].replace(' ', '')
    # s[1] is the name, s[0] is the u8 code.
    try:
        #const  escape_down  : KeyCode = KeyCode{ code: 0x01, pchar: ""};
        print("const ", s[1].upper(), " : u8 = ", s[0].lower().strip(), ";")
    except:
        print("const ", s[1].upper(), " : u8 = ", s[0].lower().strip(), ";")
