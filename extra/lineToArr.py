#!/usr/bin/python3

"""
Takes The Key codes in keyCodes.txt and
re-exports them into a text file ready for
insertion into rust.
"""

with open("keyCodes.txt") as f:
    content = f.readlines()
content = [x.strip() for x in content] # strip extra characters

for x in content:
    s = x.split(" ")
    s[0] = s[0].replace(' ', '')
    s[1] = s[1].replace(' ', '')
    # s[3] is the shifted s[2] is the printable, is[1] is the name, s[0] is the u8 code.
    #  arr elements type tuple : (KeyCodeConstU8, printableChar),
    # form (ESCAPE_DOWN, (b"?")[0])
    try:
        s[2] = s[2].replace(' ', '').strip()
        print("(", s[1].upper(), ", (b\"", s[2].lower(), "\")[0]),", sep="")
    except:
        print("(", s[1].upper(), ", (b\"?\")[0]),", sep="")