arch ?= x86_64
kernel := build/kernel-$(arch).bin
iso := build/os-$(arch).iso

linker_script := src/arch/$(arch)/linker.ld
grub_cfg := src/arch/$(arch)/grub.cfg
assembly_source_files := $(wildcard src/arch/$(arch)/*.asm)
assembly_object_files := $(patsubst src/arch/$(arch)/%.asm, build/arch/$(arch)/%.o, $(assembly_source_files))

target ?= $(arch)-custom_os
rust_os := target/$(target)/debug/libmonty_os.a

.PHONY: all clean run iso kernel doc disk

all: $(kernel)
	@echo "Done"

release: $(releaseKernel)
	@echo "Done"

clean:
	@rm -r build

run: $(iso)
	qemu-system-x86_64 -cdrom $(iso) -boot menu=on -vga std -s -serial file:serial.log -drive file=montyOS_drive_qemu,format=qcow2	

debug: $(iso)
	qemu-system-x86_64 -cdrom $(iso) -boot menu=on -vga std -s -S -serial file:serial.log -drive file=montyOS_drive_qemu,format=qcow2

gdb:
	@gdb "build/kernel-x86_64.bin" -ex "target remote :1234"

iso: $(iso)
	@echo "Done"

$(iso): $(kernel) $(grub_cfg)
	@mkdir -p build/isofiles/boot/grub
	cp $(kernel) build/isofiles/boot/kernel.bin
	cp $(grub_cfg) build/isofiles/boot/grub
	grub-mkrescue -o $(iso) build/isofiles #2> /dev/null
	@rm -r build/isofiles

$(kernel): kernel $(rust_os) $(assembly_object_files) $(linker_script)
	ld -n --gc-sections -T $(linker_script) -o $(kernel) $(assembly_object_files) $(rust_os)

kernel:
	@RUST_TARGET_PATH=$(32shell pwd) cargo xbuild --target x86_64-custom_os.json

# compile assembly files
build/arch/$(arch)/%.o: src/arch/$(arch)/%.asm
	@mkdir -p $(shell dirname $@)
	nasm -felf64 $< -o $@

doc:
	@cargo doc --open --no-deps
	@rm -rf ./doc
	@mv -f target/doc ./doc
	@echo ""
	@echo "======================="
	@echo "Doc Generation Complete."
	@echo "Documentation found in PROJECT_DIR/doc"
