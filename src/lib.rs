//! A learning experience in bare metal kernel development.
#![cfg_attr(test, allow(dead_code, unused_macros, unused_imports))]// Ignore lint warnings when testing.
#![feature(panic_handler)] // required for defining the panic handler
#![feature(extern_prelude)] // Allow use of core crate
#![feature(abi_x86_interrupt)] // Allow use of x86_64 interrupts
#![feature(const_fn)] // Enable connst functions
#![feature(lang_items)] // Allow overriding lang items
#![feature(alloc)] // No_Std core allocation library
#![feature(ptr_internals)] // Allow use of the core::ptr crate
#![feature(allocator_api)] // Allow custom allocators
#![feature(asm)]
#![feature(naked_functions)]
#![feature(plugin)]
#![no_std]

extern crate alloc;
extern crate multiboot2;
extern crate rlibc;
extern crate spin;
extern crate volatile;
#[macro_use]
extern crate bitflags;
extern crate x86_64;
#[macro_use]
extern crate once;
extern crate linked_list_allocator;
#[macro_use]
extern crate lazy_static;
extern crate bit_field;
extern crate cpuio;
extern crate uart_16550;

// This goes before all other submodules
// so they can find the macros within.

/// Contains globally used macros.
#[macro_use]
mod macros;

/// Controls text printing to the vga output.
#[macro_use]
mod vga_buffer;

/// Manager frames, paging, the stack, and the heap.
mod memory;

/// Manages CPU Interrupts: reprogramming the PIC, handling CPU exceptions, and handling IRQs.
/// Hooks into the `drivers` module to provide functionality to the rest of the kernel
#[macro_use]
mod interrupts;

/// Contains submodules that allow the kernel to interface with various physical devices.
#[macro_use]
mod drivers;

use spin::Mutex; // Spinning mutex used to manage the PIC controller.
/// Static Mutex wrapping the struct to represent the PIC.
/// use the `init()` method to set up.
static PICS: Mutex<interrupts::pic::ChainedPics> =
    Mutex::new(unsafe { interrupts::pic::ChainedPics::new(0x20, 0x28) });

#[no_mangle]
/// Main Rust Entrypoint.
/// The ASM bootstrap code jumps to here after long mode is entered.
pub extern "C" fn rust_main(multiboot_information_address: usize) {
    vga_buffer::tmode::disable_cursor();
    vga_buffer::tmode::clear_screen();
    println!("Entered rust_main");

    // Disable interrupts. using the sti instruction
    x86_64::instructions::interrupts::disable();
    println!("Disabled Interrupts");

    let boot_info = unsafe { multiboot2::load(multiboot_information_address) };
    println!("Loaded Multiboot Info");
    enable_nxe_bit();
    enable_write_protect_bit();
    println!("Enabled NXE and Write Protect Bits");

    // Set VGA pixel mode
    vga_buffer::vmode::PIX_BUF.lock().test();

    // set up guard page and map the heap pages
    let mut memory_controller = memory::init(&boot_info);

    unsafe {
        HEAP_ALLOCATOR
            .lock()
            .init(HEAP_START, HEAP_START + HEAP_SIZE);
    }
    println!("Set up Guard Page, memory_controller, and HEAP_ALLOCATOR");

    // This is okay to leave as unsafe, because we are sure we are writing to a good port.
    unsafe {
        PICS.lock().init();
    } // Initialize our PIC device.
    println!("Initialized PIC device");

    // Initialize our IDT
    interrupts::init(&mut memory_controller);
    println!("Initialized IDT");

    // Enable interrupts. using the sti instruction
    x86_64::instructions::interrupts::enable();

    println!("Successfully reenabled interrupts (sti)");

    //drivers::ata::pio::test();
    //del_col!(0);
    //loop {
    //    getln!();
    //}

    panic!("Operating System must invoke at least one module\nbefore returning from rust_main!");
}


/// Enables the NXE Bit.
/// Remember NXE enables execute-disable access rights for PAE paging and IA-32e paging
fn enable_nxe_bit() {
    use x86_64::registers::model_specific::{Efer, EferFlags};

    unsafe {
        // Read into EferFlags object, enable the NXE bit, and write to the register
        let mut efer = Efer::read();
        efer.set(EferFlags::NO_EXECUTE_ENABLE, true);
        Efer::write(efer);
    }
}

/// Enable write protection (for protected-long mode)
fn enable_write_protect_bit() {
    use x86_64::registers::control::{Cr0, Cr0Flags};

    unsafe { 
        let mut cr0_flags = Cr0::read(); 
        cr0_flags.set(Cr0Flags::WRITE_PROTECT, true);
        Cr0::write(cr0_flags);
    };
}

use core::panic::PanicInfo;
#[cfg(not(test))] // only compile when the test flag is not set
#[panic_handler]
#[no_mangle]
pub fn panic(info: &PanicInfo) -> ! {
    use vga_buffer::tmode::Color;
    change_color!(Color::Red, Color::Black);
    println!("KERNEL PANIC! {}", info);
    loop {}
}

/// Defines where the kernel heap lives
pub const HEAP_START: usize = 0o_000_001_000_000_0000;

// Defines how big the kernel heap is. (100KiB)
pub const HEAP_SIZE: usize = 100 * 1024;

#[global_allocator]
/// A linked list heap allocator, which we use as the global kernel allocator.
static HEAP_ALLOCATOR: linked_list_allocator::LockedHeap =
    linked_list_allocator::LockedHeap::empty();
