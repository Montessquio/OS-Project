use x86_64::structures::idt::{ExceptionStackFrame, PageFaultErrorCode};
use PICS;
use cpuio::{inb, outb};

// User IRQ
// Exception handlers are in `errors.rs`

pub extern "x86-interrupt" fn IRQ1_Handler(stack_frame: &mut ExceptionStackFrame) {
    //println!("STARTED IRQ1");
    unsafe {
        let scan_code = inb(0x60);
        (::drivers::kbd::KeyboardCallback.lock())(scan_code);
        outb(0x20, 0x20);
    }
    //println!("ENDED IRQ1");
}

pub extern "x86-interrupt" fn IRQ8_Handler(stack_frame: &mut ExceptionStackFrame) {
    println!("GOT IRQ8!");
    unsafe {
        PICS.lock().notify_end_of_interrupt(8);
    }
}

pub extern "x86-interrupt" fn irq_general_handler(stack_frame: &mut ExceptionStackFrame) {
    println!("GOT IRQ\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn irq_silent_handler(stack_frame: &mut ExceptionStackFrame) {}

// ATA Bus 1 handler.
pub extern "x86-interrupt" fn IRQ14_Handler(_: &mut ExceptionStackFrame) {
    println!("GOT IRQ14!");
    unsafe {
        PICS.lock().notify_end_of_interrupt(14);
    }
}

// ATA Bus 2 handler.
pub extern "x86-interrupt" fn IRQ15_Handler(stack_frame: &mut ExceptionStackFrame) {
    println!("GOT IRQ15!");
    unsafe {
        PICS.lock().notify_end_of_interrupt(15);
    }
}
