use memory::MemoryController;
use spin::Once;
use x86_64::structures::idt::{Entry, InterruptDescriptorTable, PageFaultHandlerFunc};
use x86_64::structures::tss::TaskStateSegment;
use x86_64::VirtAddr;

mod errors;
mod events;
mod gdt;
pub mod pic;

lazy_static! {
    static ref IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();

        // ERRORS REGISTRATION
        idt.breakpoint.set_handler_fn(errors::breakpoint_handler);
        idt.divide_by_zero.set_handler_fn(errors::divide_by_zero_handler);
        idt.general_protection_fault.set_handler_fn(errors::general_protection_fault_handler);
        unsafe {
            idt.double_fault.set_handler_fn(errors::double_fault_handler)
                .set_stack_index(DOUBLE_FAULT_IST_INDEX as u16);
        }

        // REMEMBER! The PIC has been remapped. Add 0x20 to any IDT vector you
        // want to change in order for it to be valid! Ex. INT1 = 0x21

        // Keyboard handler.
        idt[0x20+0x01].set_handler_fn(events::IRQ1_Handler);
        idt[0x20+0x14].set_handler_fn(events::IRQ14_Handler);
        idt[0x20+0x15].set_handler_fn(events::IRQ15_Handler);

        println!("Completed lazy_static initialization of the IDT");
        idt
    };
}

static TSS: Once<TaskStateSegment> = Once::new();
static GDT: Once<gdt::Gdt> = Once::new();

const DOUBLE_FAULT_IST_INDEX: usize = 0;

pub fn init(memory_controller: &mut MemoryController) {
    use x86_64::instructions::segmentation::set_cs;
    use x86_64::instructions::tables::load_tss;
    use x86_64::structures::gdt::SegmentSelector;

    let double_fault_stack = memory_controller
        .alloc_stack(1)
        .expect("Could not allocate double fault stack");

    let tss = TSS.call_once(|| {
        let mut tss = TaskStateSegment::new();
        tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX] =
            VirtAddr::new(double_fault_stack.top() as u64);
        tss
    });

    let mut code_selector = SegmentSelector(0);
    let mut tss_selector = SegmentSelector(0);
    let gdt = GDT.call_once(|| {
        let mut gdt = gdt::Gdt::new();
        code_selector = gdt.add_entry(gdt::Descriptor::kernel_code_segment());
        tss_selector = gdt.add_entry(gdt::Descriptor::tss_segment(&tss));
        gdt
    });
    gdt.load();

    unsafe {
        // reload code segment register
        set_cs(code_selector);
        // load TSS
        load_tss(tss_selector);
    }
    println!("Reloaded CS and Loaded TSS");

    IDT.load();
    println!("Loaded the IDT into CPU");
}
