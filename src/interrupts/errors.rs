use x86_64;
use x86_64::VirtAddr;
use x86_64::structures::idt::{InterruptDescriptorTable, ExceptionStackFrame};
use x86_64::structures::tss::TaskStateSegment;
use memory::MemoryController;
use spin::Once;
use core;
use ::vga_buffer;

// Basic Kernel Exception Handlers. No more than panic/and log to screen.
// This doesn't take care of user defined interrupts. For that, check  `events.rs`


pub extern "x86-interrupt" fn breakpoint_handler(
    stack_frame: &mut ExceptionStackFrame)
{
    println!("EXCEPTION: BREAKPOINT\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn divide_by_zero_handler(
    stack_frame: &mut ExceptionStackFrame)
{
    println!("EXCEPTION: DIVISION BY ZERO \n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn general_protection_fault_handler(
    stack_frame: &mut ExceptionStackFrame, _error_code: u64)
{
    panic!("\nEXCEPTION: GENERAL PROTECTION FAULT\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn double_fault_handler(
    stack_frame: &mut ExceptionStackFrame, _error_code: u64)
{
    panic!("\nEXCEPTION: DOUBLE FAULT\n{:#?}", stack_frame);
}


#[no_mangle]
pub extern "C" fn oom_handler(_: core::alloc::Layout) -> ! {
    #![lang = "oom"]
    panic!("OOM Error!");
}

#[lang = "eh_personality"]
extern "C" fn eh_personality() {}
