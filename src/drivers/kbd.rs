use alloc::collections::vec_deque::VecDeque;
use alloc::vec::Vec;
use cpuio;
use spin::Mutex;

// Inspired by the QuiltOS keyboard driver.
//https://wiki.osdev.org/Keyboard#Scan_Code_Set_2

// Function to convert a 8 scancode into a KeyCode
pub fn scan_to_key(code: u8) -> Option<KeyCode> {
    if !KeyboardController.lock().is_shifted {
        match KEY_CODE_TO_ASCII.get(code as usize) {
            Some(keycode) => Some(*keycode),
            None => None,
        }
    } else {
        match SHIFT_KEY_CODE_TO_ASCII.get(code as usize) {
            Some(keycode) => Some(*keycode),
            None => None,
        }
    }
}

// Primitive function to convert a keycode to a printable.
pub fn convert_code(code: KeyCode) -> Option<u8> {
    if !KeyboardController.lock().is_shifted {
        if code.1 == (b"?")[0] {
            // No output if the code is an unprintable.
            return None;
        } else {
            return Some(code.1);
        }
    } else {
        if code.1 == (b"x")[0] {
            // No output if the code is an unprintable
            return None;
        } else {
            return Some(code.1);
        }
    }
}

// These lazy_static members are seperate so they can be accessed using a mutex individually
// Got deadlocks when introducing them under one struct, so there's this now.
lazy_static! {
    // Controls the function that the INT1 handler will call.
    pub static ref KeyboardCallback: Mutex<fn (u8)> =
        Mutex::new(
            |x: u8|
            // Only register if we want input
            if KeyboardController.lock().want_input {
                if x == 0xE { // Handle DELETE key.
                    DeleteCallback.lock()();
                } else if (x == RIGHT_SHIFT_UP) || (x == LEFT_SHIFT_UP) {
                    KeyboardController.lock().is_shifted = false;
                } else if (x == RIGHT_SHIFT_DOWN) || (x == LEFT_SHIFT_DOWN) {
                    KeyboardController.lock().is_shifted = true;
                } else {
                    let key = scan_to_key(x);
                    if key.is_some() {
                        return KeyBuffer.lock().push_back(key.unwrap());
                    }
                }
            }
        ); // Default.

    // On get delete key, this callback is called.
    pub static ref DeleteCallback: Mutex<fn ()> =
        Mutex::new(
            || ::vga_buffer::tmode::delete_key()
        );

    static ref KeyBuffer: Mutex<VecDeque<KeyCode>> = Mutex::new(VecDeque::new());

    pub static ref KeyboardController : Mutex<KeyboardHandler> =
        Mutex::new(KeyboardHandler{ want_input: false, is_shifted: false, is_two_byte: false });
}

// Empty struct to allow locking functions through a mutex.
pub struct KeyboardHandler {
    pub is_two_byte: bool, // Flag for 2 byte scancodes.
    pub want_input: bool,
    pub is_shifted: bool,
}

pub fn getchar() -> u8 {
    // Wait() writes garbage data to a port that does nothing
    // This allows us to slow down the lock aquisition loop
    // just *barely* enough to make it stable.
    unsafe {
        let mut wait_port: cpuio::Port<u8> = cpuio::Port::new(0x80);
        let mut wait = || wait_port.write(0);

        // Loop around until we get a lock.
        loop {
            // Signal that we want to get input.
            KeyboardController.lock().want_input = true;
            let lock = KeyBuffer.try_lock();
            if lock.is_some() {
                let x = lock.unwrap().pop_front();
                if x.is_some() {
                    KeyboardController.lock().want_input = false;
                    let printable = convert_code(x.unwrap());
                    if printable.is_some() {
                        return printable.unwrap();
                    } else {
                        continue;
                    }
                }
            }
            // Wait a tick or so to allow other things to finish.
            wait();
        }
    }

    // Unreachable. Only here to make the compiler happy.
    return 0x0;
}

pub fn as_char(x: u8) -> char {
    return x as char;
}

pub fn getchars(count: usize) -> Vec<char> {
    let mut tmp_vec = Vec::new();
    for x in 0..count - 1 {
        tmp_vec.push(as_char(getchar()));
    }
    tmp_vec
}

pub fn getchars_echo(count: usize) -> Vec<char> {
    let mut tmp_vec = Vec::new();
    for x in 0..count {
        let y = as_char(getchar());
        for x in 0..count - 1 {
            print!("{}", y);
            tmp_vec.push(y);
        }
    }
    tmp_vec
}

// Allows a user to set a callback on each char in.
pub fn getchars_callback(count: usize, callback: fn(char)) -> Vec<char> {
    let mut tmp_vec = Vec::new();
    for x in 0..count {
        let y = as_char(getchar());
        (callback)(y);
        tmp_vec.push(y);
    }
    tmp_vec
}

// Returns on newline.
pub fn getln() -> Vec<char> {
    let mut tmp_vec = Vec::new();
    loop {
        let y = as_char(getchar());
        tmp_vec.push(y);
        if y == '\n' {
            break;
        }
    }
    tmp_vec
}

// Returns on newline.
pub fn getln_echo() -> Vec<char> {
    let mut tmp_vec: Vec<char> = Vec::new();
    loop {
        let y = as_char(getchar());
        print!("{}", y);
        tmp_vec.push(y);
        if y == '\n' {
            break;
        }
    }
    tmp_vec
}

// Calls a callback whenever a character is input
pub fn getln_callback(callback: fn(char)) -> Vec<char> {
    let mut tmp_vec = Vec::new();
    loop {
        let y = as_char(getchar());
        (callback)(y);
        tmp_vec.push(y);
        if y == '\n' {
            return tmp_vec;
        }
    }
}

macro_rules! getchar {
    // `()` indicates that the macro takes no argument.
    () => {
        // The macro will expand into the contents of this block.
        ::drivers::kbd::as_char(::drivers::kbd::getchar())
    };
}

macro_rules! getln {
    () => {
        ::drivers::kbd::getln_echo()
    };
}

fn nop() {}

/*

    Key_Codes to ASCII maps. Contains shifted and unshifted.

*/
// Keycodes >= 0x81 are key_up.

pub type KeyCode = (u8, u8);

// An array of tuples, (u8(id), char(printable))
static KEY_CODE_TO_ASCII: [KeyCode; 171] = [
    // "?" is the unprintable character here
    (NULL, (b"?")[0]),
    (ESCAPE_DOWN, (b"?")[0]),
    (ONE_DOWN, (b"1")[0]),
    (TWO_DOWN, (b"2")[0]),
    (THREE_DOWN, (b"3")[0]),
    (FOUR_DOWN, (b"4")[0]),
    (FIVE_DOWN, (b"5")[0]),
    (SIX_DOWN, (b"6")[0]),
    (SEVEN_DOWN, (b"7")[0]),
    (EIGHT_DOWN, (b"8")[0]),
    (NINE_DOWN, (b"9")[0]),
    (ZERO_DOWN, (b"0")[0]),
    (MINUS_DOWN, (b"-")[0]),
    (EQUALS_DOWN, (b"=")[0]),
    (BACKSPACE_DOWN, (b"?")[0]),
    (TAB_DOWN, (b"?")[0]),
    (Q_DOWN, (b"q")[0]),
    (W_DOWN, (b"w")[0]),
    (E_DOWN, (b"e")[0]),
    (R_DOWN, (b"r")[0]),
    (T_DOWN, (b"t")[0]),
    (Y_DOWN, (b"y")[0]),
    (U_DOWN, (b"u")[0]),
    (I_DOWN, (b"i")[0]),
    (O_DOWN, (b"o")[0]),
    (P_DOWN, (b"p")[0]),
    (OPEN_BRACKET_DOWN, (b"[")[0]),
    (CLOSE_BRACKET_DOWN, (b"]")[0]),
    (ENTER_DOWN, (b"\n")[0]),
    (LEFT_CONTROL_DOWN, (b"?")[0]),
    (A_DOWN, (b"a")[0]),
    (S_DOWN, (b"s")[0]),
    (D_DOWN, (b"d")[0]),
    (F_DOWN, (b"f")[0]),
    (G_DOWN, (b"g")[0]),
    (H_DOWN, (b"h")[0]),
    (J_DOWN, (b"j")[0]),
    (K_DOWN, (b"k")[0]),
    (L_DOWN, (b"l")[0]),
    (SEMICOLON_DOWN, (b";")[0]),
    (SINGLE_QUOTE_DOWN, (b"'")[0]),
    (BACKTICK_DOWN, (b"?")[0]),
    (LEFT_SHIFT_DOWN, (b"?")[0]),
    (BACKSLASH_DOWN, (b"\\")[0]),
    (Z_DOWN, (b"z")[0]),
    (X_DOWN, (b"x")[0]),
    (C_DOWN, (b"c")[0]),
    (V_DOWN, (b"v")[0]),
    (B_DOWN, (b"b")[0]),
    (N_DOWN, (b"n")[0]),
    (M_DOWN, (b"m")[0]),
    (COMMA_DOWN, (b",")[0]),
    (PERIOD_DOWN, (b".")[0]),
    (FORWARD_SLASH_DOWN, (b"/")[0]),
    (RIGHT_SHIFT_DOWN, (b"?")[0]),
    (KEYPAD_STAR_DOWN, (b"*")[0]),
    (LEFT_ALT_DOWN, (b"?")[0]),
    (SPACE_DOWN, (b" ")[0]),
    (CAPS_LOCK_DOWN, (b"?")[0]),
    (F1_DOWN, (b"?")[0]),
    (F2_DOWN, (b"?")[0]),
    (F3_DOWN, (b"?")[0]),
    (F4_DOWN, (b"?")[0]),
    (F5_DOWN, (b"?")[0]),
    (F6_DOWN, (b"?")[0]),
    (F7_DOWN, (b"?")[0]),
    (F8_DOWN, (b"?")[0]),
    (F9_DOWN, (b"?")[0]),
    (F10_DOWN, (b"?")[0]),
    (NUM_LOCK_DOWN, (b"?")[0]),
    (SCROLL_LOCK_DOWN, (b"?")[0]),
    (KEYPAD_7_DOWN, (b"7")[0]),
    (KEYPAD_8_DOWN, (b"8")[0]),
    (KEYPAD_9_DOWN, (b"9")[0]),
    (KEYPAD_MINUS_DOWN, (b"-")[0]),
    (KEYPAD_4_DOWN, (b"4")[0]),
    (KEYPAD_5_DOWN, (b"5")[0]),
    (KEYPAD_6_DOWN, (b"6")[0]),
    (KEYPAD_PLUS_DOWN, (b"+")[0]),
    (KEYPAD_1_DOWN, (b"1")[0]),
    (KEYPAD_2_DOWN, (b"2")[0]),
    (KEYPAD_3_DOWN, (b"3")[0]),
    (KEYPAD_0_DOWN, (b"0")[0]),
    (KEYPAD_PERIOD_DOWN, (b".")[0]),
    (F11_DOWN, (b"?")[0]),
    (F12_DOWN, (b"?")[0]),
    (ESCAPE_UP, (b"?")[0]),
    (ONE_UP, (b"?")[0]),
    (TWO_UP, (b"?")[0]),
    (THREE_UP, (b"?")[0]),
    (FOUR_UP, (b"?")[0]),
    (FIVE_UP, (b"?")[0]),
    (SIX_UP, (b"?")[0]),
    (SEVEN_UP, (b"?")[0]),
    (EIGHT_UP, (b"?")[0]),
    (NINE_UP, (b"?")[0]),
    (ZERO_UP, (b"?")[0]),
    (MINUS_UP, (b"?")[0]),
    (EQUALS_UP, (b"?")[0]),
    (BACKSPACE_UP, (b"?")[0]),
    (TAB_UP, (b"?")[0]),
    (Q_UP, (b"?")[0]),
    (W_UP, (b"?")[0]),
    (E_UP, (b"?")[0]),
    (R_UP, (b"?")[0]),
    (T_UP, (b"?")[0]),
    (Y_UP, (b"?")[0]),
    (U_UP, (b"?")[0]),
    (I_UP, (b"?")[0]),
    (O_UP, (b"?")[0]),
    (P_UP, (b"?")[0]),
    (OPEN_BRACKET_UP, (b"?")[0]),
    (CLOSE_BRACKET_UP, (b"?")[0]),
    (ENTER_UP, (b"?")[0]),
    (LEFT_CONTROL_UP, (b"?")[0]),
    (A_UP, (b"?")[0]),
    (S_UP, (b"?")[0]),
    (D_UP, (b"?")[0]),
    (F_UP, (b"?")[0]),
    (G_UP, (b"?")[0]),
    (H_UP, (b"?")[0]),
    (J_UP, (b"?")[0]),
    (K_UP, (b"?")[0]),
    (L_UP, (b"?")[0]),
    (SEMICOLON_UP, (b"?")[0]),
    (SINGLE_QUOTE_UP, (b"?")[0]),
    (BACKTICK_UP, (b"?")[0]),
    (LEFT_SHIFT_UP, (b"?")[0]),
    (BACKSLASH_UP, (b"?")[0]),
    (Z_UP, (b"?")[0]),
    (X_UP, (b"?")[0]),
    (C_UP, (b"?")[0]),
    (V_UP, (b"?")[0]),
    (B_UP, (b"?")[0]),
    (N_UP, (b"?")[0]),
    (M_UP, (b"?")[0]),
    (COMMA_UP, (b"?")[0]),
    (PERIOD_UP, (b"?")[0]),
    (FORWARD_SLASH_UP, (b"?")[0]),
    (RIGHT_SHIFT_UP, (b"?")[0]),
    (KEYPAD_STAR_UP, (b"?")[0]),
    (LEFT_ALT_UP, (b"?")[0]),
    (SPACE_UP, (b"?")[0]),
    (CAPS_LOCK_UP, (b"?")[0]),
    (F1_UP, (b"?")[0]),
    (F2_UP, (b"?")[0]),
    (F3_UP, (b"?")[0]),
    (F4_UP, (b"?")[0]),
    (F5_UP, (b"?")[0]),
    (F6_UP, (b"?")[0]),
    (F7_UP, (b"?")[0]),
    (F8_UP, (b"?")[0]),
    (F9_UP, (b"?")[0]),
    (F10_UP, (b"?")[0]),
    (NUM_LOCK_UP, (b"?")[0]),
    (SCROLL_LOCK_UP, (b"?")[0]),
    (KEYPAD_7_UP, (b"?")[0]),
    (KEYPAD_8_UP, (b"?")[0]),
    (KEYPAD_9_UP, (b"?")[0]),
    (KEYPAD_MINUS_UP, (b"?")[0]),
    (KEYPAD_4_UP, (b"?")[0]),
    (KEYPAD_5_UP, (b"?")[0]),
    (KEYPAD_6_UP, (b"?")[0]),
    (KEYPAD_PLUS_UP, (b"?")[0]),
    (KEYPAD_1_UP, (b"?")[0]),
    (KEYPAD_2_UP, (b"?")[0]),
    (KEYPAD_3_UP, (b"?")[0]),
    (KEYPAD_0_UP, (b"?")[0]),
    (KEYPAD_PERIOD_UP, (b"?")[0]),
    (F11_UP, (b"?")[0]),
    (F12_UP, (b"?")[0]),
];

static SHIFT_KEY_CODE_TO_ASCII: [KeyCode; 171] = [
    // Lowercase "x" is the unprintable here.
    (NULL, (b"x")[0]),
    (ESCAPE_DOWN, (b"x")[0]),
    (ONE_DOWN, (b"!")[0]),
    (TWO_DOWN, (b"@")[0]),
    (THREE_DOWN, (b"#")[0]),
    (FOUR_DOWN, (b"$")[0]),
    (FIVE_DOWN, (b"%")[0]),
    (SIX_DOWN, (b"^")[0]),
    (SEVEN_DOWN, (b"&")[0]),
    (EIGHT_DOWN, (b"*")[0]),
    (NINE_DOWN, (b"(")[0]),
    (ZERO_DOWN, (b")")[0]),
    (MINUS_DOWN, (b"_")[0]),
    (EQUALS_DOWN, (b"+")[0]),
    (BACKSPACE_DOWN, (b"x")[0]),
    (TAB_DOWN, (b"x")[0]),
    (Q_DOWN, (b"Q")[0]),
    (W_DOWN, (b"W")[0]),
    (E_DOWN, (b"E")[0]),
    (R_DOWN, (b"R")[0]),
    (T_DOWN, (b"T")[0]),
    (Y_DOWN, (b"Y")[0]),
    (U_DOWN, (b"U")[0]),
    (I_DOWN, (b"I")[0]),
    (O_DOWN, (b"O")[0]),
    (P_DOWN, (b"P")[0]),
    (OPEN_BRACKET_DOWN, (b"{")[0]),
    (CLOSE_BRACKET_DOWN, (b"}")[0]),
    (ENTER_DOWN, (b"\n")[0]),
    (LEFT_CONTROL_DOWN, (b"x")[0]),
    (A_DOWN, (b"A")[0]),
    (S_DOWN, (b"S")[0]),
    (D_DOWN, (b"D")[0]),
    (F_DOWN, (b"F")[0]),
    (G_DOWN, (b"G")[0]),
    (H_DOWN, (b"H")[0]),
    (J_DOWN, (b"J")[0]),
    (K_DOWN, (b"K")[0]),
    (L_DOWN, (b"L")[0]),
    (SEMICOLON_DOWN, (b":")[0]),
    (SINGLE_QUOTE_DOWN, (b"\"")[0]),
    (BACKTICK_DOWN, (b"~")[0]),
    (LEFT_SHIFT_DOWN, (b"x")[0]),
    (BACKSLASH_DOWN, (b"|")[0]),
    (Z_DOWN, (b"Z")[0]),
    (X_DOWN, (b"X")[0]),
    (C_DOWN, (b"C")[0]),
    (V_DOWN, (b"V")[0]),
    (B_DOWN, (b"B")[0]),
    (N_DOWN, (b"N")[0]),
    (M_DOWN, (b"M")[0]),
    (COMMA_DOWN, (b"<")[0]),
    (PERIOD_DOWN, (b">")[0]),
    (FORWARD_SLASH_DOWN, (b"?")[0]),
    (RIGHT_SHIFT_DOWN, (b"x")[0]),
    (KEYPAD_STAR_DOWN, (b"x")[0]),
    (LEFT_ALT_DOWN, (b"x")[0]),
    (SPACE_DOWN, (b" ")[0]),
    (CAPS_LOCK_DOWN, (b"x")[0]),
    (F1_DOWN, (b"x")[0]),
    (F2_DOWN, (b"x")[0]),
    (F3_DOWN, (b"x")[0]),
    (F4_DOWN, (b"x")[0]),
    (F5_DOWN, (b"x")[0]),
    (F6_DOWN, (b"x")[0]),
    (F7_DOWN, (b"x")[0]),
    (F8_DOWN, (b"x")[0]),
    (F9_DOWN, (b"x")[0]),
    (F10_DOWN, (b"x")[0]),
    (NUM_LOCK_DOWN, (b"x")[0]),
    (SCROLL_LOCK_DOWN, (b"x")[0]),
    (KEYPAD_7_DOWN, (b"x")[0]),
    (KEYPAD_8_DOWN, (b"x")[0]),
    (KEYPAD_9_DOWN, (b"x")[0]),
    (KEYPAD_MINUS_DOWN, (b"x")[0]),
    (KEYPAD_4_DOWN, (b"x")[0]),
    (KEYPAD_5_DOWN, (b"x")[0]),
    (KEYPAD_6_DOWN, (b"x")[0]),
    (KEYPAD_PLUS_DOWN, (b"x")[0]),
    (KEYPAD_1_DOWN, (b"x")[0]),
    (KEYPAD_2_DOWN, (b"x")[0]),
    (KEYPAD_3_DOWN, (b"x")[0]),
    (KEYPAD_0_DOWN, (b"x")[0]),
    (KEYPAD_PERIOD_DOWN, (b"x")[0]),
    (F11_DOWN, (b"x")[0]),
    (F12_DOWN, (b"x")[0]),
    (ESCAPE_UP, (b"x")[0]),
    (ONE_UP, (b"x")[0]),
    (TWO_UP, (b"x")[0]),
    (THREE_UP, (b"x")[0]),
    (FOUR_UP, (b"x")[0]),
    (FIVE_UP, (b"x")[0]),
    (SIX_UP, (b"x")[0]),
    (SEVEN_UP, (b"x")[0]),
    (EIGHT_UP, (b"x")[0]),
    (NINE_UP, (b"x")[0]),
    (ZERO_UP, (b"x")[0]),
    (MINUS_UP, (b"x")[0]),
    (EQUALS_UP, (b"x")[0]),
    (BACKSPACE_UP, (b"x")[0]),
    (TAB_UP, (b"x")[0]),
    (Q_UP, (b"x")[0]),
    (W_UP, (b"x")[0]),
    (E_UP, (b"x")[0]),
    (R_UP, (b"x")[0]),
    (T_UP, (b"x")[0]),
    (Y_UP, (b"x")[0]),
    (U_UP, (b"x")[0]),
    (I_UP, (b"x")[0]),
    (O_UP, (b"x")[0]),
    (P_UP, (b"x")[0]),
    (OPEN_BRACKET_UP, (b"x")[0]),
    (CLOSE_BRACKET_UP, (b"x")[0]),
    (ENTER_UP, (b"x")[0]),
    (LEFT_CONTROL_UP, (b"x")[0]),
    (A_UP, (b"x")[0]),
    (S_UP, (b"x")[0]),
    (D_UP, (b"x")[0]),
    (F_UP, (b"x")[0]),
    (G_UP, (b"x")[0]),
    (H_UP, (b"x")[0]),
    (J_UP, (b"x")[0]),
    (K_UP, (b"x")[0]),
    (L_UP, (b"x")[0]),
    (SEMICOLON_UP, (b"x")[0]),
    (SINGLE_QUOTE_UP, (b"x")[0]),
    (BACKTICK_UP, (b"x")[0]),
    (LEFT_SHIFT_UP, (b"x")[0]),
    (BACKSLASH_UP, (b"x")[0]),
    (Z_UP, (b"x")[0]),
    (X_UP, (b"x")[0]),
    (C_UP, (b"x")[0]),
    (V_UP, (b"x")[0]),
    (B_UP, (b"x")[0]),
    (N_UP, (b"x")[0]),
    (M_UP, (b"x")[0]),
    (COMMA_UP, (b"x")[0]),
    (PERIOD_UP, (b"x")[0]),
    (FORWARD_SLASH_UP, (b"x")[0]),
    (RIGHT_SHIFT_UP, (b"x")[0]),
    (KEYPAD_STAR_UP, (b"x")[0]),
    (LEFT_ALT_UP, (b"x")[0]),
    (SPACE_UP, (b"x")[0]),
    (CAPS_LOCK_UP, (b"x")[0]),
    (F1_UP, (b"x")[0]),
    (F2_UP, (b"x")[0]),
    (F3_UP, (b"x")[0]),
    (F4_UP, (b"x")[0]),
    (F5_UP, (b"x")[0]),
    (F6_UP, (b"x")[0]),
    (F7_UP, (b"x")[0]),
    (F8_UP, (b"x")[0]),
    (F9_UP, (b"x")[0]),
    (F10_UP, (b"x")[0]),
    (NUM_LOCK_UP, (b"x")[0]),
    (SCROLL_LOCK_UP, (b"x")[0]),
    (KEYPAD_7_UP, (b"x")[0]),
    (KEYPAD_8_UP, (b"x")[0]),
    (KEYPAD_9_UP, (b"x")[0]),
    (KEYPAD_MINUS_UP, (b"x")[0]),
    (KEYPAD_4_UP, (b"x")[0]),
    (KEYPAD_5_UP, (b"x")[0]),
    (KEYPAD_6_UP, (b"x")[0]),
    (KEYPAD_PLUS_UP, (b"x")[0]),
    (KEYPAD_1_UP, (b"x")[0]),
    (KEYPAD_2_UP, (b"x")[0]),
    (KEYPAD_3_UP, (b"x")[0]),
    (KEYPAD_0_UP, (b"x")[0]),
    (KEYPAD_PERIOD_UP, (b"x")[0]),
    (F11_UP, (b"x")[0]),
    (F12_UP, (b"x")[0]),
];

/*

Const Keycode Identifiers

*/

// Scan Set 1
const NULL: u8 = 0x00;
const ESCAPE_DOWN: u8 = 0x01;
const ONE_DOWN: u8 = 0x02;
const TWO_DOWN: u8 = 0x03;
const THREE_DOWN: u8 = 0x04;
const FOUR_DOWN: u8 = 0x05;
const FIVE_DOWN: u8 = 0x06;
const SIX_DOWN: u8 = 0x07;
const SEVEN_DOWN: u8 = 0x08;
const EIGHT_DOWN: u8 = 0x09;
const NINE_DOWN: u8 = 0x0A;
const ZERO_DOWN: u8 = 0x0B;
const MINUS_DOWN: u8 = 0x0C;
const EQUALS_DOWN: u8 = 0x0D;
const BACKSPACE_DOWN: u8 = 0x0E;
const TAB_DOWN: u8 = 0x0F;
const Q_DOWN: u8 = 0x10;
const W_DOWN: u8 = 0x11;
const E_DOWN: u8 = 0x12;
const R_DOWN: u8 = 0x13;
const T_DOWN: u8 = 0x14;
const Y_DOWN: u8 = 0x15;
const U_DOWN: u8 = 0x16;
const I_DOWN: u8 = 0x17;
const O_DOWN: u8 = 0x18;
const P_DOWN: u8 = 0x19;
const OPEN_BRACKET_DOWN: u8 = 0x1A;
const CLOSE_BRACKET_DOWN: u8 = 0x1B;
const ENTER_DOWN: u8 = 0x1C;
const LEFT_CONTROL_DOWN: u8 = 0x1D;
const A_DOWN: u8 = 0x1E;
const S_DOWN: u8 = 0x1F;
const D_DOWN: u8 = 0x20;
const F_DOWN: u8 = 0x21;
const G_DOWN: u8 = 0x22;
const H_DOWN: u8 = 0x23;
const J_DOWN: u8 = 0x24;
const K_DOWN: u8 = 0x25;
const L_DOWN: u8 = 0x26;
const SEMICOLON_DOWN: u8 = 0x27;
const SINGLE_QUOTE_DOWN: u8 = 0x28;
const BACKTICK_DOWN: u8 = 0x29;
const LEFT_SHIFT_DOWN: u8 = 0x2A;
const BACKSLASH_DOWN: u8 = 0x2B;
const Z_DOWN: u8 = 0x2C;
const X_DOWN: u8 = 0x2D;
const C_DOWN: u8 = 0x2E;
const V_DOWN: u8 = 0x2F;
const B_DOWN: u8 = 0x30;
const N_DOWN: u8 = 0x31;
const M_DOWN: u8 = 0x32;
const COMMA_DOWN: u8 = 0x33;
const PERIOD_DOWN: u8 = 0x34;
const FORWARD_SLASH_DOWN: u8 = 0x35;
const RIGHT_SHIFT_DOWN: u8 = 0x36;
const KEYPAD_STAR_DOWN: u8 = 0x37;
const LEFT_ALT_DOWN: u8 = 0x38;
const SPACE_DOWN: u8 = 0x39;
const CAPS_LOCK_DOWN: u8 = 0x3A;
const F1_DOWN: u8 = 0x3B;
const F2_DOWN: u8 = 0x3C;
const F3_DOWN: u8 = 0x3D;
const F4_DOWN: u8 = 0x3E;
const F5_DOWN: u8 = 0x3F;
const F6_DOWN: u8 = 0x40;
const F7_DOWN: u8 = 0x41;
const F8_DOWN: u8 = 0x42;
const F9_DOWN: u8 = 0x43;
const F10_DOWN: u8 = 0x44;
const NUM_LOCK_DOWN: u8 = 0x45;
const SCROLL_LOCK_DOWN: u8 = 0x46;
const KEYPAD_7_DOWN: u8 = 0x47;
const KEYPAD_8_DOWN: u8 = 0x48;
const KEYPAD_9_DOWN: u8 = 0x49;
const KEYPAD_MINUS_DOWN: u8 = 0x4A;
const KEYPAD_4_DOWN: u8 = 0x4B;
const KEYPAD_5_DOWN: u8 = 0x4C;
const KEYPAD_6_DOWN: u8 = 0x4D;
const KEYPAD_PLUS_DOWN: u8 = 0x4E;
const KEYPAD_1_DOWN: u8 = 0x4F;
const KEYPAD_2_DOWN: u8 = 0x50;
const KEYPAD_3_DOWN: u8 = 0x51;
const KEYPAD_0_DOWN: u8 = 0x52;
const KEYPAD_PERIOD_DOWN: u8 = 0x53;
const F11_DOWN: u8 = 0x57;
const F12_DOWN: u8 = 0x58;
const ESCAPE_UP: u8 = 0x81;
const ONE_UP: u8 = 0x82;
const TWO_UP: u8 = 0x83;
const THREE_UP: u8 = 0x84;
const FOUR_UP: u8 = 0x85;
const FIVE_UP: u8 = 0x86;
const SIX_UP: u8 = 0x87;
const SEVEN_UP: u8 = 0x88;
const EIGHT_UP: u8 = 0x89;
const NINE_UP: u8 = 0x8A;
const ZERO_UP: u8 = 0x8B;
const MINUS_UP: u8 = 0x8C;
const EQUALS_UP: u8 = 0x8D;
const BACKSPACE_UP: u8 = 0x8E;
const TAB_UP: u8 = 0x8F;
const Q_UP: u8 = 0x90;
const W_UP: u8 = 0x91;
const E_UP: u8 = 0x92;
const R_UP: u8 = 0x93;
const T_UP: u8 = 0x94;
const Y_UP: u8 = 0x95;
const U_UP: u8 = 0x96;
const I_UP: u8 = 0x97;
const O_UP: u8 = 0x98;
const P_UP: u8 = 0x99;
const OPEN_BRACKET_UP: u8 = 0x9A;
const CLOSE_BRACKET_UP: u8 = 0x9B;
const ENTER_UP: u8 = 0x9C;
const LEFT_CONTROL_UP: u8 = 0x9D;
const A_UP: u8 = 0x9E;
const S_UP: u8 = 0x9F;
const D_UP: u8 = 0xA0;
const F_UP: u8 = 0xA1;
const G_UP: u8 = 0xA2;
const H_UP: u8 = 0xA3;
const J_UP: u8 = 0xA4;
const K_UP: u8 = 0xA5;
const L_UP: u8 = 0xA6;
const SEMICOLON_UP: u8 = 0xA7;
const SINGLE_QUOTE_UP: u8 = 0xA8;
const BACKTICK_UP: u8 = 0xA9;
const LEFT_SHIFT_UP: u8 = 0xAA;
const BACKSLASH_UP: u8 = 0xAB;
const Z_UP: u8 = 0xAC;
const X_UP: u8 = 0xAD;
const C_UP: u8 = 0xAE;
const V_UP: u8 = 0xAF;
const B_UP: u8 = 0xB0;
const N_UP: u8 = 0xB1;
const M_UP: u8 = 0xB2;
const COMMA_UP: u8 = 0xB3;
const PERIOD_UP: u8 = 0xB4;
const FORWARD_SLASH_UP: u8 = 0xB5;
const RIGHT_SHIFT_UP: u8 = 0xB6;
const KEYPAD_STAR_UP: u8 = 0xB7;
const LEFT_ALT_UP: u8 = 0xB8;
const SPACE_UP: u8 = 0xB9;
const CAPS_LOCK_UP: u8 = 0xBA;
const F1_UP: u8 = 0xBB;
const F2_UP: u8 = 0xBC;
const F3_UP: u8 = 0xBD;
const F4_UP: u8 = 0xBE;
const F5_UP: u8 = 0xBF;
const F6_UP: u8 = 0xC0;
const F7_UP: u8 = 0xC1;
const F8_UP: u8 = 0xC2;
const F9_UP: u8 = 0xC3;
const F10_UP: u8 = 0xC4;
const NUM_LOCK_UP: u8 = 0xC5;
const SCROLL_LOCK_UP: u8 = 0xC6;
const KEYPAD_7_UP: u8 = 0xC7;
const KEYPAD_8_UP: u8 = 0xC8;
const KEYPAD_9_UP: u8 = 0xC9;
const KEYPAD_MINUS_UP: u8 = 0xCA;
const KEYPAD_4_UP: u8 = 0xCB;
const KEYPAD_5_UP: u8 = 0xCC;
const KEYPAD_6_UP: u8 = 0xCD;
const KEYPAD_PLUS_UP: u8 = 0xCE;
const KEYPAD_1_UP: u8 = 0xCF;
const KEYPAD_2_UP: u8 = 0xD0;
const KEYPAD_3_UP: u8 = 0xD1;
const KEYPAD_0_UP: u8 = 0xD2;
const KEYPAD_PERIOD_UP: u8 = 0xD3;
const F11_UP: u8 = 0xD7;
const F12_UP: u8 = 0xD8;
