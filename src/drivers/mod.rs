// This module exposes basic information about drivers. Submodules define the drivers themselves.
// Note, do not confuse this module as controlling all drivers.
// It simply sets up some kernel-required bare-metal drivers.
// Module-based drivers may be used later in the init process.

#[macro_use]
pub mod kbd; // Primitive US QWERTY keyboard driver.
pub mod ata;

#[macro_use]
pub mod serial; // Serial port I/O.
