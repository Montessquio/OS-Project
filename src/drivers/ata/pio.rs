/* From the OSDEV Wiki's ATA Page:
According to the ATA specs, PIO mode must always be
supported by all ATA-compliant drives as the default
data transfer mechanism.

PIO mode uses a tremendous amount of CPU resources,
because every byte of data transferred between the
disk and the CPU must be sent through the CPU's IO
port bus (not the memory). On some CPUs, PIO mode
can still achieve actual transfer speeds of 16MB per sec,
but no other processes on the machine will get any CPU time.

 However, when a computer is just beginning to boot
 there are no other processes.
 So PIO mode is an excellent and simple interface to utilize during bootup,
 until the system goes into multitasking mode.
*/

/*
Current disk controller chips almost always support two ATA buses per chip.
There is a standardized set of IO ports to control the disks on the buses.
The first two buses are called the Primary and Secondary ATA bus,
and are almost always controlled by IO ports 0x1F0 through 0x1F7, a
nd 0x170 through 0x177, respectively (unless you change it).
The associated Device Control Registers/Alternate Status ports are
IO ports 0x3F6, and 0x376, respectively.
The standard IRQ for the Primary bus is IRQ14, and IRQ15 for the Secondary bus.

If the next two buses exist, they are normally controlled
by IO ports 0x1E8 through 0x1EF, and 0x168 through 0x16F, respectively.
The associated Device Control Registers/Alternate Status ports are IO ports 0x3E6, and 0x366.

The actual control registers and IRQs for each bus can often be determined by
enumerating the PCI bus, finding all the disk controllers, and reading the
information from each controller's PCI Configuration Space.
So, technically, PCI enumeration should be done before ATA device detection.
However, this method is not exactly reliable.

When the system boots, according to the specs,
the PCI disk controller is supposed to be in "
Legacy/Compatibility" mode. This means it is
supposed to use the standardized IO port settings.
You may have no real choice but to rely on that fact.
*/

/*
Port    Offset 	                        Function 	Description 	                                Param. size LBA28/LBA48
0 	    Data Port 	                    Read/Write PIO data bytes on this port. 	                16-bit / 16-bit
1 	    Features / Error Information 	Usually used for ATAPI devices. 	                        8-bit / 16-bit
2 	    Sector Count 	                Number of sectors to read/write (0 is a special value). 	8-bit / 16-bit
3 	    Sector Number / LBAlo 	        This is CHS / LBA28 / LBA48 specific. 	                    8-bit / 16-bit
4 	    Cylinder Low / LBAmid 	        Partial Disk Sector address. 	                            8-bit / 16-bit
5 	    Cylinder High / LBAhi 	        Partial Disk Sector address. 	                            8-bit / 16-bit
6 	    Drive / Head Port 	            Used to select a drive and/or head.                        	8-bit / 8-bit
7 	    Command port / Reg. Stat port 	Used to send commands or read the current status. 	        8-bit / 8-bit
*/

/*

Bit 	Abbreviation 	Function
0 	    ERR 	Indicates an error occurred. Send a new command to clear it (or nuke it with a Software Reset).
3 	    DRQ 	Set when the drive has PIO data to transfer, or is ready to accept PIO data.
4 	    SRV 	Overlapped Mode Service Request.
5 	    DF 	    Drive Fault Error (does not set ERR).
6 	    RDY 	Bit is clear when drive is spun down, or after an error. Set otherwise.
7 	    BSY 	Indicates the drive is preparing to send/receive data (wait for it to clear). In case of 'hang' (it never clears), do a software reset.
*/
// Bit masks to find given Status Register flags.
const ATA_SR_ERR: u8 = 0b0000_0001;
const ATA_SR_DRQ: u8 = 0b0000_1000;
const ATA_SR_SRV: u8 = 0b0001_0000;
const ATA_SR_DF : u8 = 0b0010_0000;
const ATA_SR_RDY: u8 = 0b0100_0000;
const ATA_SR_BSY: u8 = 0b1000_0000;

// ATA-Commands:
const ATA_CMD_READ_PIO          : u8 = 0x20;
const ATA_CMD_READ_PIO_EXT      : u8 = 0x24;
const ATA_CMD_READ_DMA          : u8 = 0xC8;
const ATA_CMD_READ_DMA_EXT      : u8 = 0x25;
const ATA_CMD_WRITE_PIO         : u8 = 0x30;
const ATA_CMD_WRITE_PIO_EXT     : u8 = 0x34;
const ATA_CMD_WRITE_DMA         : u8 = 0xCA;
const ATA_CMD_WRITE_DMA_EXT     : u8 = 0x35;
const ATA_CMD_CACHE_FLUSH       : u8 = 0xE7;
const ATA_CMD_CACHE_FLUSH_EXT   : u8 = 0xEA;
const ATA_CMD_PACKET            : u8 = 0xA0;
const ATA_CMD_IDENTIFY_PACKET   : u8 = 0xA1;
const ATA_CMD_IDENTIFY          : u8 = 0xEC;

// ATAPI Commands
const ATAPI_CMD_READ    : u8 = 0xA8;
const ATAPI_CMD_EJECT   : u8 = 0x1B;

// ERR Codes
const ATA_ER_BBK    : u8 = 0x80;
const ATA_ER_UNC    : u8 = 0x40;
const ATA_ER_MC     : u8 = 0x20;
const ATA_ER_IDNF   : u8 = 0x10;
const ATA_ER_MCR    : u8 = 0x08;
const ATA_ER_ABRT   : u8 = 0x04;
const ATA_ER_TK0NF  : u8 = 0x02;
const ATA_ER_AMNF   : u8 = 0x01;

// The Commands ATA_CMD_IDENTIFY_PACKET, and ATA_CMD_IDENTIFY
// return a buffer of 512 bytes.
// The buffer is called Identification space, the following definitions
// are used to read information from the identification space.
const ATA_IDENT_DEVICETYPE  : u8 = 0;
const ATA_IDENT_CYLINDERS   : u8 = 2;
const ATA_IDENT_HEADS       : u8 = 6;
const ATA_IDENT_SECTORS     : u8 = 12;
const ATA_IDENT_SERIAL      : u8 = 20;
const ATA_IDENT_MODEL       : u8 = 54;
const ATA_IDENT_CAPABILITIES: u8 = 98;
const ATA_IDENT_FIELDVALID  : u8 = 106;
const ATA_IDENT_MAX_LBA     : u8 = 120;
const ATA_IDENT_COMMANDSETS : u8 = 164;
const ATA_IDENT_MAX_LBA_EXT : u8 = 200;

// Identify drives as the master or slave
const ATA_MASTER: u8 = 0x00;
const ATA_SLAVE : u8 = 0x01;

// Define ATA and ATAPI mode.
const IDE_ATA   : u8 = 0x00;
const IDE_ATAPI : u8 = 0x01;

// ATA-ATAPI Task-File:
// Task File is a range of ports [8 ports]
// which are used by primary channel [BAR0] or Secondary Channel [BAR2].
const ATA_REG_DATA      : u8 = 0x00;
const ATA_REG_ERROR     : u8 = 0x01;
const ATA_REG_FEATURES  : u8 = 0x01;
const ATA_REG_SECCOUNT0 : u8 = 0x02;
const ATA_REG_LBA0      : u8 = 0x03;
const ATA_REG_LBA1      : u8 = 0x04;
const ATA_REG_LBA2      : u8 = 0x05;
const ATA_REG_HDDEVSEL  : u8 = 0x06;
const ATA_REG_COMMAND   : u8 = 0x07;
const ATA_REG_STATUS    : u8 = 0x07;
const ATA_REG_SECCOUNT1 : u8 = 0x08;
const ATA_REG_LBA3      : u8 = 0x09;
const ATA_REG_LBA4      : u8 = 0x0A;
const ATA_REG_LBA5      : u8 = 0x0B;
const ATA_REG_CONTROL   : u8 = 0x0C;
const ATA_REG_ALTSTATUS : u8 = 0x0C;
const ATA_REG_DEVADDRESS: u8 = 0x0D;

// ATA Control channels
// Channels:
const ATA_PRIMARY   : u8 = 0x00;
const ATA_SECONDARY : u8 = 0x01;

// Directions:
const ATA_READ  : u8 = 0x00;
const ATA_WRITE : u8 = 0x01;

// Contains information of one ATA Bus.
// This is going to be LBA28 mode.
struct Channel {
    base: u16, // Base port.
    sector_count: u16,
    status: status_byte,

    ctrl: u8,  // Control Base
    bmide: u8, // Bus Master IDE
    nIEN: u8,  // nIEN (No Interrupt);
}

use cpuio::{inb, inw, outb, outw};

use cpuio;
impl Channel {
    pub fn new(p: u16) -> Channel {
        let mut b = Channel {
            base: p,
            sector_count: 0,
            status: status_byte::new(p),

            ctrl: 0,
            bmide: 0,
            nIEN: 0,
        };

        b.sector_count = b.get_sectors();

        b
    }

    fn get_sectors(&mut self) -> u16 {
        unsafe {
            let x = inw(ATA_REG_SECCOUNT0 as u16);
            if x == 0 {
                return 256;
            } else {
                return x;
            }
        } // Offset 2 is Sector Count
    }

    pub unsafe fn ide_read(&self, reg: u8) -> u8 {
        let mut result: u8 = 0;
        if reg > 0x07 && reg < 0x0C {
            self.ide_write(ATA_REG_CONTROL, 0x80 | self.nIEN);
        }
        if reg < 0x08 {
            result = inb(self.base + reg as u16 - 0x00);
        } else if reg < 0x0C {
            result = inb(self.base + reg as u16 - 0x06);
        } else if reg < 0x0E {
            result = inb(self.base + reg as u16 - 0x0A);
        } else if reg < 0x16 {
            result = inb(self.base + reg as u16 - 0x0E);
        }
        if reg > 0x07 && reg < 0x0C {
            self.ide_write(ATA_REG_CONTROL, self.nIEN);
        }
        return result;
    }

    pub unsafe fn ide_write(&self, reg: u8, data: u8) {
        if reg > 0x07 && reg < 0x0C {
            self.ide_write(ATA_REG_CONTROL, 0x80 | self.nIEN);
        }
        if reg < 0x08 {
            outb(data, self.base + reg as u16 - 0x00);
        } else if reg < 0x0C {
            outb(data, (self.base + reg as u16 - 0x06) as u16);
        } else if reg < 0x0E {
            outb(data, (self.ctrl + reg - 0x0A) as u16);
        } else if reg < 0x16 {
            outb(data, (self.bmide + reg - 0x0E) as u16);
        }
        if reg > 0x07 && reg < 0x0C {
            self.ide_write(ATA_REG_CONTROL, self.nIEN);
        }
    }

    pub fn wait(&self) {
        // It's okay for this to be in an unsafe block,
        // because we are only *reading* the alt status register.
        unsafe {
            self.ide_read(ATA_REG_ALTSTATUS); // Reading Alternate Status Port wastes 100ns.
            self.ide_read(ATA_REG_ALTSTATUS); // Reading Alternate Status Port wastes 100ns.
            self.ide_read(ATA_REG_ALTSTATUS); // Reading Alternate Status Port wastes 100ns.
            self.ide_read(ATA_REG_ALTSTATUS); // Reading Alternate Status Port wastes 100ns.
        }
    }

    fn ide_print_err(&self, mut err: u8) -> u8 {
        unsafe {
            if err == 0 {
            return err;
            }
            //let cur_col = ::vga_buffer::get_color();
            change_color!(::vga_buffer::tmode::Color::Red, ::vga_buffer::tmode::Color::Black);
            print!(" IDE:");
            if err == 1 {
                print!("- Device Fault\n     ");
                err = 19;
            } else if err == 2 {
                let st: u8 = self.ide_read(ATA_REG_ERROR);
                if st & ATA_ER_AMNF == ATA_ER_AMNF {
                    print!("- No Address Mark Found\n     ");
                    err = 7;
                }
                if st & ATA_ER_TK0NF == ATA_ER_TK0NF{
                    print!("- No Media or Media Error\n     ");
                    err = 3;
                }
                if st & ATA_ER_ABRT == ATA_ER_ABRT{
                    print!("- Command Aborted\n     ");
                    err = 20;
                }
                if st & ATA_ER_MCR == ATA_ER_MCR{
                    print!("- No Media or Media Error\n     ");
                    err = 3;
                }
                if st & ATA_ER_IDNF == ATA_ER_IDNF{
                    print!("- ID mark not Found\n     ");
                    err = 21;
                }
                if st & ATA_ER_MC == ATA_ER_MC{
                    print!("- No Media or Media Error\n     ");
                    err = 3;
                }
                if st & ATA_ER_UNC == ATA_ER_UNC {
                    print!("- Uncorrectable Data Error\n     ");
                    err = 22;
                }
                if st & ATA_ER_BBK == ATA_ER_BBK {
                    print!("- Bad Sectors\n     ");
                    err = 13;
                }
            } else if err == 3 {
                print!("- Reads Nothing\n     ");
                err = 23;
            } else if err == 4 {
                print!("- Write Protected\n     ");
                err = 8;
            }
            print!("{}\n", self);

            // Cleanup
            //set_color!(cur_col);

            // End
            return err;
        }
    }
}

pub fn test() {
    let mut b = Channel::new(0x1F0);
    println!("{}", b);
}

// Contains boolean info about the ATA status byte.
#[derive(Debug)]
struct status_byte {
    port: u16,
    pub flags: u8,
    pub ERR: bool,
    pub DRQ: bool,
    pub SRV: bool,
    pub DF: bool,
    pub RDY: bool,
    pub BSY: bool,
}

impl status_byte {
    pub fn new(base_port: u16) -> status_byte {
        let mut sb = status_byte {
            port: base_port,
            flags: 0b0000_0000,
            ERR: false,
            DRQ: false,
            SRV: false,
            DF: false,
            RDY: false,
            BSY: false,
        };

        sb.update();

        sb
    }

    fn check_err(&self) -> bool {
        if (self.flags & ATA_SR_ERR) == ATA_SR_ERR {
            return true;
        } else {
            return false;
        }
    }

    fn check_drq(&self) -> bool {
        // Check for the DRQ bit.
        if (self.flags & ATA_SR_DRQ) == ATA_SR_DRQ {
            return true;
        } else {
            return false;
        }
    }

    fn check_srv(&self) -> bool {
        if (self.flags & ATA_SR_SRV) == ATA_SR_SRV {
            return true;
        } else {
            return false;
        }
    }

    fn check_df(&self) -> bool {
        if (self.flags & ATA_SR_DF) == ATA_SR_DF {
            return true;
        } else {
            return false;
        }
    }

    fn check_rdy(&self) -> bool {
        if (self.flags & ATA_SR_RDY) == ATA_SR_RDY {
            return true;
        } else {
            return false;
        }
    }

    fn check_bsy(&self) -> bool {
        if (self.flags & ATA_SR_BSY) == ATA_SR_BSY {
            return true;
        } else {
            return false;
        }
    }

    fn check_flags(&self) -> u8 {
        unsafe {
            return inb(self.port + 7);
        }
    }

    pub fn update(&mut self) {
        unsafe {
            self.flags = self.check_flags();

            // Mask out each bit and store it's result.
             /*
             0 	ERR 	Indicates an error occurred. Send a new command to clear it (or nuke it with a Software Reset).
             3 	DRQ 	Set when the drive has PIO data to transfer, or is ready to accept PIO data.
             4 	SRV 	Overlapped Mode Service Request.
             5 	DF 	    Drive Fault Error (does not set ERR).
             6 	RDY 	Bit is clear when drive is spun down, or after an error. Set otherwise.
             7 	BSY 	Indicates the drive is preparing to send/receive data (wait for it to clear). In case of 'hang' (it never clears), do a software reset.
             */

            // Check for the ERR bit.
            self.ERR = self.check_err();

            // Check for the DRQ bit.
            self.DRQ = self.check_drq();

            // Check the SRV bit.
            self.SRV = self.check_srv();

            // Check the DF bit.
            self.DF = self.check_df();

            // Check the RDY bit.
            self.RDY = self.check_rdy();

            // Check the BSY bit.
            self.BSY = self.check_bsy();

            // Done checking flags.
        }
    }
}

use core::fmt;
impl fmt::Display for status_byte {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ERR: {}    ", self.ERR);
        write!(f, "DRQ: {}    ", self.DRQ);
        write!(f, "SRV: {}    \n", self.SRV);
        write!(f, "DF: {}     ", self.DF);
        write!(f, "RDY: {}    ", self.RDY);
        write!(f, "BSY: {}    ", self.BSY);
        return Ok(());
    }
}

impl fmt::Display for Channel {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Base: {} | Sectors: {} | CTRLBase: {} | Bus Master IDE: {} | nIEN: {}\nFlags:\n{}",
            self.base, self.sector_count, self.ctrl, self.bmide, self.nIEN, self.status
        );

        return Ok(());
    }
}
