//! Main File for OS VGA Video Output
//! Written by Nicolas "Montessquio" Suarez
//! Licensed under the GNU GPL V.3 -
//! For more information see LICENSE
//! 
//! From https://web.archive.org/web/20140218012818/http://atschool.eduweb.co.uk/camdean/pupils/amac/vga.htm
//!
//! Structure of mode 13h 
//! In mode 13h, the screen is 320 pixels in 
//! width and 200 pixels in height. 
//! This is mapped 0 to 319 on the x axis and 0 to 199 
//! on the y axis, with the origin (0,0) at the top-left corner
//! Since this is a 256-color mode, each pixel represents 
//! 8 bits (28=256) or one byte, so the memory needed is 
//! 320*200 or 64,000 bytes. 
//!
//! Since memory is linear (unlike the computer screen, 
//! which has both an x and a y dimension), the offset 
//! into computer memory must be calculated to plot a pixel. 
//! To do this the y value is multiplied by the width of 
//! the screen, or 320, and the x value is added to that. 
//! Thus to plot a pixel at location (256,8), first calculate 
//! 256+8*320=2816 or B00h, then write to segment A000h, 
//! offset B00h. The following program segment creates a 
//! pointer to address A000:0000h, computes the offset 
//! from two variables, and then writes to the calculated 
//! memory location.

use core::ptr::Unique;
use spin::Mutex;
use volatile::Volatile;

/// Contains the start of the Pixel Buffer.
/// Runs 64,000 bytes beyond
const PXBUF_START: usize = 0xA0000;


/// Calculate / set size of vga buffer, in pixels.
const BUFFER_HEIGHT: usize = 200; 
const BUFFER_WIDTH: usize = 320; 


/// PX_WRITER allows safe manipulation of the VGA pixel buffer. 
/// Stored in a spinlock to prevent races and all that.
pub static PIX_BUF: Mutex<PxWriter> = Mutex::new(PxWriter {
    buffer: unsafe { Unique::new_unchecked(PXBUF_START as *mut _) },
});

/// Unique, unsafe, buffer wrapper
/// By specifying the specific ranges
/// of this screen buffer, we can prevent
/// memory writes to undesired places.
struct PixBuf {
    pub pixels: [[Volatile<[u8; 3]>; BUFFER_WIDTH]; BUFFER_HEIGHT]
}

/// px_writer provides access to the VGA pixel buffer.
pub struct PxWriter {
  buffer: Unique<PixBuf>,
}

impl PxWriter {
    pub fn test(&mut self) {
        for row in 0..BUFFER_HEIGHT {
            for column in 0..BUFFER_WIDTH {
                self.buffer().pixels[row][column].write(rgb(102, 153, 255));
            }
        }
    }

    /// Convert the raw Buffer pointer to a safe buffer interface.
    fn buffer(&mut self) -> &mut PixBuf {
        unsafe { self.buffer.as_mut() }
    }

}

/// Packs three u8s into a [u8; 3] while ensuring endianness compatibility.
pub fn rgb(r: u8, g: u8, b: u8) -> [u8; 3] {
    [b, g, r]
}