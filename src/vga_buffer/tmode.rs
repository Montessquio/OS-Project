/*
*=> Main File for OS VGA Text Output
*=> Written by Nicolas "Montessquio" Suarez
*=> Licensed under the GNU GPL V.3 -
*=> For more information see LICENSE
*/

use core::fmt;
use core::ptr::Unique;
use spin::Mutex;
use volatile::Volatile;


#[allow(dead_code)] // Not all the code here is going to be reachable, since it's a library.
#[derive(Debug, Clone, Copy)] // Color Objects should be copy-able for use as arguments.
#[repr(u8)] // These color codes are to be represented as unsigned 8-bit values.
pub enum Color {
    // Enumerate color names to codes.
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
}


#[derive(Debug, Clone, Copy)]
pub struct ColorCode(u8); // Struct to hold a color code

impl ColorCode {
    // Compose said codes using a method of ColorCode
    const fn new(foreground: Color, background: Color) -> ColorCode {
        // A `Color` is a 4 bit value. Therefore, two colors can be put into one byte (bg, fg)
        // Bit shift the background down 4, and then OR it with the foreground (resulting in the combining of the two).
        ColorCode((background as u8) << 4 | (foreground as u8))
    }
}


// A spinning mutex allows us to "lock" a resource even though we have no threads yet.
// Using this, we can make thread-safe print()s without threads
pub static WRITER: Mutex<Writer> = Mutex::new(Writer {
    // Store a WRITER object in a spinning mutex.
    column_position: 0,
    color_code: ColorCode::new(Color::White, Color::Black),
    buffer: unsafe { Unique::new_unchecked(0xb8000 as *mut _) },
    min_del_col: 0,
});


#[derive(Debug, Clone, Copy)]
#[repr(C)] // Used so struct is saved in order of elements
struct ScreenChar {
    // Safely stores one complete character.
    ascii_character: u8,   // A single ascii character - a one byte value.
    color_code: ColorCode, // another byte, (bg(u4) + fg(u4))
}

// Calculate / set size of vga buffer
const BUFFER_HEIGHT: usize = 25;
const BUFFER_WIDTH: usize = 80;

// This is a buffer containing all screen characters.
struct Buffer {
    // A volatile object is never optimized by the compiler.
    // This means our screenbuffer won't be corrupted because of rust's optimization
    chars: [[Volatile<ScreenChar>; BUFFER_WIDTH]; BUFFER_HEIGHT],
}

// Struct/method to write to buffer.
pub struct Writer {
    column_position: usize, // Current column
    color_code: ColorCode,  // What the ColorCode of our current row,col is.
    buffer: Unique<Buffer>, // Unique indicates that Writer owns Buffer even though it's not explicitly stated.
    pub min_del_col: usize, // What column to prevent deletes.
}

impl Writer {
    // Print a single char to the screen.
    pub fn write_byte(&mut self, byte: u8) {
        match byte {
            b'\n' => self.new_line(), // If it's a newline or if we've reached the end of the line...
            byte => {
                // Draw a newline using the appropriate function.
                if self.column_position >= BUFFER_WIDTH {
                    self.new_line();
                }

                // Calculate where the character should go. (ROW, COL position)
                let row = BUFFER_HEIGHT - 1;
                let col = self.column_position;

                let color_code = self.color_code;

                // Actually place the character into the buffer object.
                self.buffer().chars[row][col].write(ScreenChar {
                    ascii_character: byte,
                    color_code: color_code,
                });

                // Increment the position in anticipation of the next char.
                self.column_position += 1;
            }
        }
    }

    // Write a full string to the buffer.
    pub fn write_str(&mut self, s: &str) {
        // String to byte array -> loop through & print every byte
        for byte in s.bytes() {
            self.write_byte(byte)
        }
    }

    // Convert the raw Buffer pointer to a safe buffer interface.
    fn buffer(&mut self) -> &mut Buffer {
        unsafe { self.buffer.as_mut() }
    }

    // Print a new line... That is to say move each row up by one, clearing the final line for writing.
    fn new_line(&mut self) {
        for row in 1..BUFFER_HEIGHT {
            for col in 0..BUFFER_WIDTH {
                let buffer = self.buffer();
                let character = buffer.chars[row][col].read();
                buffer.chars[row - 1][col].write(character);
            }
        }
        self.clear_row(BUFFER_HEIGHT - 1);
        self.column_position = 0;
    }

    // Clear the whole row by overwriting it with spaces.
    fn clear_row(&mut self, row: usize) {
        let blank = ScreenChar {
            ascii_character: b' ',
            color_code: self.color_code,
        };
        for col in 0..BUFFER_WIDTH {
            self.buffer().chars[row][col].write(blank);
        }
    }

    // Set color code to one designed out of this module.
    fn color_change(&mut self, cc: ColorCode) {
        self.color_code = cc;
    }

    // Delete the last character in the buffer, and change the position to there.
    pub fn del_byte(&mut self) {
        // Draw a newline using the appropriate function.
        if self.column_position - 1 < self.min_del_col {
            return; // don't handle cases where we need to shift *down*
        }

        // Calculate where the character should go. (ROW, COL position)
        let row = BUFFER_HEIGHT - 1;

        self.column_position -= 1;
        let col = self.column_position;

        let color_code = self.color_code;

        // Actually place the character into the buffer object.
        self.buffer().chars[row][col].write(ScreenChar {
            ascii_character: 0x0,
            color_code: color_code,
        });
    }
}

//  Add support for rust auto-formatting macros.
impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for byte in s.bytes() {
            self.write_byte(byte)
        }
        Ok(())
    }
}

// Rust macro functions to print quickly and efficiently.
macro_rules! printv {
    ($($arg:tt)*) => ({
        $crate::vga_buffer::tmode::print(format_args!($($arg)*));
    });
}

// This is the same as the last one, except only add a newline to the end.
macro_rules! printvln {
    ($fmt:expr) => (printv!(concat!($fmt, "\n")));
    ($fmt:expr, $($arg:tt)*) => (printv!(concat!($fmt, "\n"), $($arg)*));
}

// Rust macro to change the color of the screenchars.
macro_rules! change_color {
    ($($arg:tt)*) => ({
        $crate::vga_buffer::tmode::change_color($($arg)*);
    });
}

macro_rules! set_color {
    ($($arg:tt)*) => ({
        $crate::vga_buffer::tmode::set_color($($arg)*);
    });
}

// Prevent race conditions and deadlocks in concurrent code.
pub fn print(args: fmt::Arguments) {
    use core::fmt::Write;
    WRITER.lock().write_fmt(args).unwrap();
}

// Change terminal text color.
pub fn change_color(foreground: Color, background: Color) {
    WRITER
        .lock()
        .color_change(ColorCode::new(foreground, background));
}

// Set color using a ColorCode instance.
pub fn set_color(cc: ColorCode) {
    WRITER.lock().color_code = cc;
}

// Get current console color.
pub fn get_color() -> ColorCode {
    let x = WRITER.lock().color_code;
    return x;
}

// Use the macros to clear the screen efficiently.
// Clear screen by printing a number of empty lines equal to the amount of rows in the screen.1111111
pub fn clear_screen() {
    for _ in 0..BUFFER_HEIGHT {
        println!("");
    }
}

pub fn set_del_col(col: usize) {
    WRITER.lock().min_del_col = col;
}

macro_rules! del_col {
    ($($arg:tt)*) => ({
        $crate::vga_buffer::tmode::set_del_col($($arg)*);
    });
}

pub fn delete_key() {
    WRITER.lock().del_byte();
}

use cpuio::outb;
pub fn enable_cursor() {}

pub fn disable_cursor() {
    // It's okay for this to be marked as unsafe because the ports do nothing but control the cursor.
    unsafe {
        outb(0x3D4, 0x0A);
        outb(0x3D5, 0x20);
    }
}
