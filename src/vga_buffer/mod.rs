//! VGA Text and pixel mode manipulation
//! Also, code common between both of them.

/// Text mode code
#[macro_use]
pub mod tmode;

/// Video (pixel) mode code
#[macro_use]
pub mod vmode;
