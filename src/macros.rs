/// Print macro puts both to vga and serial.
macro_rules! print {
    ($($arg:tt)*) => ({
        $crate::vga_buffer::tmode::print(format_args!($($arg)*));
        $crate::drivers::serial::print(format_args!($($arg)*));
    });
}

/// Same as print! but appends a newline.
macro_rules! println {
    () => ( print!("\n") );
    ($fmt:expr) => ( print!(concat!($fmt, "\n")) );
    ($fmt:expr, $($arg:tt)*) => ( print!(concat!($fmt, "\n"), $($arg)*) );
}
