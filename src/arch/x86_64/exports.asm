; Exported assembly function definitions.
; No code here is called from assembly code.
; It should *only* be called from Rust

global example_func
section .text
bits 32

example_func:
  ret

; To set the video mode, call interrupt 10h
; with 0 (zero) in the AH register and the 
; desired mode number in the AL register.
;set_text_mode:
;  push BL ; General purpose reg to help with MOV
;  push AH
;  push AL
; 
;  mov BL, 0x00 
;  mov AH, BL
;
;  mov BL, 0x03
;  mov AL, BL 
;  int 0x10
;
;  pop AL
;  pop AH
;  pop BL
;  ret
;
;set_video_mode:
;  push BL ; General purpose reg to help with MOV
;  push AH
;  push AL
; 
;  mov BL, 0x00 
;  mov AH, BL
;
;  mov BL, 0x13
;  mov AL, BL 
;  int 0x10
;
;  pop AL
;  pop AH
;  pop BL
;  ret
