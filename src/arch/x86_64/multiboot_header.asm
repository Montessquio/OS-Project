section .multiboot_header
header_start:
    dd 0xe85250d6                ; magic number (multiboot 2)
    dd 0                         ; architecture 0 (protected mode i386)
    dd header_end - header_start ; header length
    ; checksum
    dd 0x100000000 - (0xe85250d6 + 0 + (header_end - header_start))

    ; insert optional multiboot tags here
	
	; VBE Mode tag
    ;         +-------------------+
    ; u16     | type = 5          |
    ; u16     | flags             |
    ; u32     | size = 20         |
    ; u32     | width             |
    ; u32     | height            |
    ; u32     | depth             |
    ;         +-------------------+
	dw 5
	dw 0
	dd 20
	dd 0
	dd 0
	dd 0
	dd 8

    ; required end tag
    dw 0    ; type
    dw 0    ; flags
    dd 8    ; size
header_end:
